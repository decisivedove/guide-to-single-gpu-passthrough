Moved to [a new URL](https://gitlab.com/decisivedove/guide-to-single-gpu-passthrough/-/wikis/). 

# Guide To Single GPU Passthrough

This article will go over setting up single GPU Passthrough on Arch Linux. The guide is mainly for amd cards but I have included notes for Nvidia users. This is currently a work in progress guide.
There is this common myth that two GPUs are needed for GPU passthrough. That is simply not true. Single GPU passthrough deserves more recogntion. This enables you to run Windows and other proprietary garbage with native graphics performance without feeling guilty of running proprietary garbage on bare hardware.

Step 1:
Get required packages
Install: `libvirt virt-manager edk2-ovmf qemu`
It is advisable to install optional dependencies for libvirt to get networking and other stuff working. At the time of writing this article, those packages are:
```
dmidecode (optional) - DMI system info support
dnsmasq (optional) - required for default NAT/DHCP for guests
ebtables (optional) - required for default NAT networking
gettext (optional) - required for libvirt-guests.service
libvirt-storage-gluster (optional) - Gluster storage backend
libvirt-storage-iscsi-direct (optional) - iSCSI-direct storage backend
libvirt-storage-rbd (optional) - RBD storage backend
lvm2 (optional) - Logical Volume Manager support
open-iscsi (optional) - iSCSI support via iscsiadm
openbsd-netcat (optional) - for remote management over ssh
qemu (optional) - QEMU/KVM support
radvd (optional) - IPv6 RAD support
```

The most up to date list can be found at https://www.archlinux.org/packages/community/x86_64/libvirt/ .

Step 2:
Load vfio kernel modules on boot.
Firstly edit the file `/etc/mkinitcpio.conf` in your favorite text editor (I know it is Vim. If it is nano then read my guide for setting up a Ubuntu VM in Windows)
You will see a line with MODULES=(...). It may be empty. Make sure to add `amdgpu vfio_pci vfio vfio_iommu_type1 vfio_virqfd` to the parenthesis. The amdgpu module must be loaded before any vfio module or else your GPU may not work in the host. Be sure to keep a chroot environment handy in case you break something.
As an example: `MODULES=(amdgpu vfio_pci vfio vfio_iommu_type1 vfio_virqfd)`
Finally save the file and exit.

Note to nvidia users: Use `nvidia` intead of `amdgpu` so you will have  `nvidia vfio_pci vfio vfio_iommu_type1 vfio_virqfd` 

Now we need to identify the IOMMU group that contains your GPU. Run the IOMMU groups script that is in the same repository. It will list all the IOMMU groups in your system. It will have a VGA compatible controller as well as an audio device. For example:
```
IOMMU Group 17:
01:00.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon RX 470/480/570/570X/580/580X/590] [1002:67df] (rev ef)

01:00.1 Audio device [0403]: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere HDMI Audio [Radeon RX 470/480 / 570/580/590] [1002:aaf0]
```
*`Note: If there are other things in the IOMMU Group, you will need to passthrough them as well. Use your descretion as to whether or not the device is essential. If you cannot passthrough those devices, consider using an ACS Override to break up the IOMMU Groups. See the section on ACS Override.`*

You want the address inside "[ ]"s. In my example, it is 1002:67df and 1002:aaf0.

Now you need to create another file: `/etc/modprobe.d/vfio.conf`. In the file add the following lines:
```
options vfio-pci ids=1002:67df, 1002:aaf0
options vfio-pci disable_vga=1
```
Be sure to include the pci ids of your devices you want to passthrough that we determined by reading the output of the IOMMU_groups script. You can add an infinite amount of devices in the list seperated by a ", ".

(OPTIONAL) While you are at it, you might want to set kvm ignore msrs in the KVM. Edit the file `/etc/modprobe.d/vfio.conf` (If it doesn't exist, create it). Add these two lines (or uncomment them if they are already there):

```
options kvm ignore_msrs=1
options kvm report_ignored_msrs=0
```


Once you are done with this, run `sudo mkinitcpio -P linux`

Step 4: Enable IOMMU in GRUB. First, make sure that IOMMU is enabled in BIOS. While you are at it, also make sure that virtualization(SVM,VT-x,Intel VT-d, etc. depending on your motherboard).
Next edit the file `/etc/default/grub`:
Where it says `GRUB_CMDLINE_LINUX_DEFAULT="..."` , make sure to add `amd_iommu=on iommu=pt` to the list of GRUB commandline parameters. If have an Intel CPU, use `intel_iommu=on` instead of `amd_iommu=on`.
If you use another bootloader other than GRUB, read the documentation on how to add a kernel boot commandline parameter.

Step 5: Create a regular VM with UEFI using ovmf with virt-manager.
In order to do so, run virt-manager. Go to file, and select "New Virtual Machine".

Step 6 :

Step 7 : Here is where the magic happens,

**ACS Override**
This Override will break up IOMMU groups effectively. It is usually not recommonded to use it if your IOMMU groups are already in good shape i.e. the devices for your graphics card are in their own group or they contain something else that you can safely passthrough to the VM.
You will need a custom kernel for this: 
https://aur.archlinux.org/packages/linux-vfio/
https://aur.archlinux.org/packages/linux-vfio-headers/

If for some reason you cannot compile the kernel from source, both the package mantainers have a custom repository (Read the Pinned Comments in: https://aur.archlinux.org/packages/linux-vfio/)

Once you have installed the kernel, add the following kernel parameter in your bootloader : `pcie_acs_override=downstream,multifunction`
For GRUB, you need to edit `/etc/default/grub` and add it inside the quotes in `GRUB_CMDLINE_LINUX_DEFAULT="... pcie_acs_override=downstream,multifunction ..." ` (Note: Do not add "..." in the GRUB config) . Then update your bootloader. For grub run this command: `sudo grub-mkconfig -o /boot/grub/grub.cfg`. Then while booting, select the vfio kernel or make it your default kernel. Once done, run the IOMMU_groups script again and magic!
